"""El precio de venta de un automóvil nuevo para el comprador, es la suma total del
costo de fabricación del vehículo, del porcentaje de ganancia del vendedor y de los
impuestos de venta establecidos. Supóngase una ganancia del vendedor del 17% sobre
el costo de fabricación del vehículo y impuesto de ventas del 13% también sobre el
costo del vehículo más la ganancia. Diseñe un algoritmo que reciba el costo del
auto e imprima el precio de venta para el consumidor."""

precioVehiculo= int(input("digite el precio del vehiculo deseado : "))

ganancia= precioVehiculo*0.17

impuesto= (precioVehiculo+ganancia)*0.13

total=precioVehiculo+impuesto+ganancia
print("el precio de su vehiculo tiene un total de" ,total)

