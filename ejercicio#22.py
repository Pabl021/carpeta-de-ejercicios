"""Un coleccionista y vendedor de carros antiguos desea imprimir a un cliente la lista
de los carros que tiene su empresa y que cumplan con la condición que dicho cliente
pide. Autos con número de placa entre 5000 y 8000, y que sean modelo 1958 o 1959.
Diseñe un algoritmo que reciba como entrada el número de placa y el año de un auto e
imprima la si el vehículo podría o no interesarle al cliente."""

placa=int(input("digite su placa"))
annio=int(input("digite su año del vehiculo"))

if placa >=5000 and placa<=8000 :
    if (annio == 1958) or (annio==1959):
        print("este auto te podria interesar")
    else:
        print("este auto no es de su agrado")
else:
        print("este auto no es de su agrado")
