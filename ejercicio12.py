"""Cree un diagrama de flujo que dado un año A indique si es o no bisiesto. Un año es
bisiesto si es múltiplo de 4, exceptuando los múltiplos de 100, que sólo son bisiestos
cuando son múltiplos de 400, por ejemplo el año 1900 no fue bisiesto, pero el año
2000 si lo fue."""

annio=int(input("ingrese el año que desea saber si es visciesto o no : "))

if annio % 4==0 :
    if annio% 100==0:
        if annio%400==0:
            print("el año es bisciesto")
        else:
            print("este año no es bisciesto")
    else:
        print("este año es bisciesto")
else:
    print("este año no es bisciesto")

