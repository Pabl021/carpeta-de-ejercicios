"""Diseñe un algoritmo que reciba como entradas las horas trabajadas y el salario por
hora de un trabajador y calcule su salario neto tomando en cuenta que: si el salario bruto es
menor o igual a ¢100 000 se le rebaja un 10% de impuesto, pero si el salario bruto es
mayor a ¢100 000 se le rebajará un 10% sobre los primeros 100 000 y un 15% sobre el
exceso de los ¢100 000. El sistema debe imprimir el salario neto (salario bruto – monto del
impuesto). """

horasTrab= int(input("digite sus horas trabajadas : "))
salarioXHora=int(input("digite su salario por horas : "))

SBruto= horasTrab*salarioXHora

if SBruto<= 100000:
    impuesto= SBruto* 0.10
    SalarioNeto= SBruto-impuesto
    print("el salario neto es de" , SalarioNeto)

else:
    rebajo= 10000
    otrorebajo= SalarioNeto-100000
    segundoimpuesto=otrorebajo*0.15
    neto2=SBruto-rebajo-segundoimpuesto
    print("el salario neto es de" , neto2)
