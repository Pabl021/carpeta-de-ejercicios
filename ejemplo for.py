
"""for i in "hola mundo":
    print(i, end="\n")

for i in range(10):
    print(i*2, end=" , ")

for i in range(15,10,-1):
    print(i, end="\n")"""

#EJERCICIOS DE WHILE

#PARA QUE LOS NUMEROS VAYAN DE MAYOR A MENOR
num=5
while num>0:
    print(num)
    num=num-1
print("fin del ciclo")

#cuando quiero que pare digito 0
opcion=1
while opcion!=0:
    opcion=int(input("ingrese un numero nuevo"))

#PARA QUE EMPIECE DE 1 A 5
num=0
while num<6:
    print(num)
    num=num+1
print("fin del ciclo")
