"""Diseñe un algoritmo que reciba el largo y el ancho de un terreno y calcule el área; si
dicha área es mayor a 100 m2, entonces el sistema deberá imprimir como salida “Terreno
apto para construcción”, caso contrario se imprimirá “Terreno no apto para construcción”."""

largo=int(input("digite el largo de su terreno en el que desea construir : "))
ancho=int(input("digite el ancho de su terreno donde se realizara la construccion : "))

medida=largo*ancho

if medida > 100:
    print("Terreno apto para construcción")
else:
    print("Terreno no apto para construcción")