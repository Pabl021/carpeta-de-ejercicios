"""Diseñe un algoritmo que reciba como entradas el peso de un cargamento y calcule el
monto a pagar por su transporte, tomando en cuenta que: si el peso total es menor a 50
toneladas, cada una cuesta 1000 colones, si es mayor o igual a 50 toneladas, cada una de
ellas cuesta 1500."""

pesoCargamento=int(input("digite el peso de su cargamento que compro : "))

if pesoCargamento < 50:
    print("el precio a pagar es de 1000 colones")
elif pesoCargamento >=50:
    print("el precio a pagar es de 1500 colones")